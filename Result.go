package Result

import (
	"fmt"
)

type Result struct {
	Comment string
	Err     error
	Res     int
}

func Rrint() {
	fmt.Println("Hello, Result!")
}

/*
12345
*/
